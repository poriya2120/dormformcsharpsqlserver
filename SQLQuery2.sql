USE [khobgah]
GO
/****** Object:  StoredProcedure [dbo].[bkdatabases]    Script Date: 01/28/2021 17:16:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[bkdatabases]
@address nvarchar(3000)
as

backup database khobgah to disk=@address




USE [khobgah]
GO
/****** Object:  StoredProcedure [dbo].[deleteclercks]    Script Date: 01/28/2021 17:16:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[deleteclercks]
@calerkId int
as
if exists(select * from calerks where calerkID=@calerkId)
begin
	delete  from calerks where calerkID=@calerkId
	return 1
end

else
return 0



USE [khobgah]
GO
/****** Object:  StoredProcedure [dbo].[editclercks]    Script Date: 01/28/2021 17:16:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [dbo].[editclercks]
@calerkId int,
@name int,
@family nvarchar(100),
@username nvarchar(100),
@phone int,
@addres nvarchar(100),
@descrptions nvarchar(100),
@passwords nvarchar(100)

as
if not exists( select  * from calerks where calerkID=@calerkId)
return 0

else
update  calerks  set  name=@name,family=@family,username=@username,phone=@phone,addres=@addres,descriptions=@descrptions,passwords=@passwords
where calerkID=@calerkId
return 1


USE [khobgah]
GO
/****** Object:  StoredProcedure [dbo].[insertclercks]    Script Date: 01/28/2021 17:17:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[insertclercks]
@calerkId int,
@name  nvarchar(100),
@family nvarchar(100),
@username nvarchar(100),
@phone int,
@addres nvarchar(100),
@descrptions nvarchar(100),
@passwords nvarchar(100)

as
if( select  COUNT(*) from calerks where calerkID=@calerkId)>0
return 0

else
insert into calerks  values(@calerkId,@name,@family,@username,@phone,@addres,@descrptions,@passwords)
return 1


USE [khobgah]
GO
/****** Object:  StoredProcedure [dbo].[insertStudent]    Script Date: 01/28/2021 17:17:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[insertStudent]
@studentId int,
@roomid int,
@name nvarchar(50),
@family nvarchar(50),
@phone int,
@addres nvarchar(50)

as
if( select  COUNT(*) from students where sudentId=@studentId)>0
return 0

else
insert into students  values(@studentId,@roomid,@name,@family,@phone,@addres)


USE [khobgah]
GO
/****** Object:  StoredProcedure [dbo].[listStudent]    Script Date: 01/28/2021 17:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[listStudent]

as
 select * from students
 
 
 USE [khobgah]
GO
/****** Object:  StoredProcedure [dbo].[ps_showCalerk]    Script Date: 01/28/2021 17:17:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[ps_showCalerk]

as

select * from rezerve


USE [khobgah]
GO
/****** Object:  StoredProcedure [dbo].[serchclercks]    Script Date: 01/28/2021 17:18:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[serchclercks]
@calerkId int,
@Name nvarchar(100) output
as


if not exists( select  * from calerks where calerkID=@calerkId)
return 0
else
set @Name=(select name from calerks where @calerkId=calerkID)
return 1



USE [khobgah]
GO
/****** Object:  StoredProcedure [dbo].[Sp_AddOrEdit]    Script Date: 01/28/2021 17:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[Sp_AddOrEdit]

@basporsid int,
@roomid int,
@khobgahid int,
@basporsname nvarchar(100),
@qualityroom nvarchar(100),
@descrptions nvarchar(100)
as
begin
if not exists(
	select * from basporsi where basporsid=@basporsid
	)
begin
	insert into basporsi 
		values(@basporsid,@roomid,@khobgahid,@basporsname,@qualityroom,@descrptions)
	
end
else
begin
	update basporsi set
	 roomid=@roomid,
	 khobgahid=@khobgahid,
	 basporsname=@basporsname,
	 qualityroom=@qualityroom,
	 descrptions=@descrptions
		where basporsid=@basporsid
	 
end

end


USE [khobgah]
GO
/****** Object:  StoredProcedure [dbo].[Sp_deleteByID]    Script Date: 01/28/2021 17:19:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[Sp_deleteByID]

@basporsid int

as
begin
	delete from basporsi where basporsid=@basporsid
end



USE [khobgah]
GO
/****** Object:  StoredProcedure [dbo].[SP_insertRezerve]    Script Date: 01/28/2021 17:20:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[SP_insertRezerve]
@rezervekId int,
@roomId int,
@studentId int,
@calerktId int,
@name  nvarchar(100),
@family nvarchar(100),
@phone int,
@addres nvarchar(100)


as
if( select  COUNT(*) from rezerve where rezerveId=@rezervekId)>0
return 0

else
insert into calerks  values(@rezervekId,@roomId,@studentId,@calerktId,@name,@family,@phone,@addres)
return 1


USE [khobgah]
GO
/****** Object:  StoredProcedure [dbo].[Sp_serachAllOrSerchId]    Script Date: 01/28/2021 17:20:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[Sp_serachAllOrSerchId]

@basporsname nvarchar(100)

as
begin
	select * from basporsi where @basporsname=''
		
		or
			
			basporsname like '%'+@basporsname+'%'
end