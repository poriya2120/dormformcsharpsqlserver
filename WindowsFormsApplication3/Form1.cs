﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Media;

namespace WindowsFormsApplication3
{
    public partial class Form1 : Form
    {
        public void attach()
        {
            SqlConnection con = new SqlConnection("Data Source=.;Initial Catalog=master;Integrated Security=True");
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandText = "select * from sysdatabases where name='khobgah'";
            con.Open();
            SqlDataReader read = cmd.ExecuteReader();
            if (!read.Read())
            {
                SqlConnection con2 = new SqlConnection();
                SqlCommand cmd2 = new SqlCommand("Data Source=.;Initial Catalog=master;Integrated Security=True");
                cmd2.Connection = con2;

                string str = Application.StartupPath + "/dat/";
                cmd2.CommandText = "exec sp_attach_db @dbname=N'khobgah',@filename1=N'" + str + "khobgah.mdf',@filename2=N'" + str + "khobgah_log.ldf'";



            }
            con.Close();

        }
        /// <summary>
        /// ///----------------------backup----------------------
        /// </summary>
        public void backup()
        {
            SaveFileDialog s = new SaveFileDialog();
            if (s.ShowDialog() == DialogResult.OK)
            {
                SqlConnection sqlcon = new SqlConnection("Data Source=.;Initial Catalog=khobgah;Integrated Security=True");
                SqlCommand sqlcmd = new SqlCommand();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Connection = sqlcon;

                sqlcmd.CommandText = "bkdatabases";
                sqlcmd.Parameters.AddWithValue("@address", s.FileName);

                sqlcon.Open();
                sqlcmd.ExecuteNonQuery();
                MessageBox.Show("attach sussess");
            }
        }

        //------------------------------------------------------------------------
        public void restore()
        {
            try
            {

                OpenFileDialog op = new OpenFileDialog();
                if (op.ShowDialog() == DialogResult.OK)
                {
                    SqlConnection sqlcon = new SqlConnection("Data Source=.;Initial Catalog=master;Integrated Security=True");
                    SqlCommand sqlcmd = new SqlCommand();
                    sqlcmd.CommandText = "use master  alter database khobgah set single_user with rollback immediate  restore database khobgah from disk='" + op.FileName + "' with replace ";

                    sqlcmd.Connection = sqlcon;

                    sqlcon.Open();
                    sqlcmd.ExecuteNonQuery();

                    sqlcon.Close();
                }
                MessageBox.Show("restore is sussess");

            }
            catch
            {
                MessageBox.Show("have error");
            }
        }
        public Form1()
        {
            InitializeComponent();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Form3 f2 = new Form3();
            f2.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form4 f4 = new Form4();
            f4.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            restore();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            backup();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Form5 f5 = new Form5();
            f5.Show();
        }
        SoundPlayer sp = new SoundPlayer(@"C:\summer.wav");
        private void Form1_Load(object sender, EventArgs e)
        {
           
            sp.PlayLooping();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Form7 f7 = new Form7();
            f7.Show();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            Form6 f6 = new Form6();
            f6.Show();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Form8 f8 = new Form8();
            f8.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            sp.Stop();
        }

        private void button14_Click(object sender, EventArgs e)
        {
            Form13 f13 = new Form13();
            f13.Show();
        }

        private void button13_Click(object sender, EventArgs e)
        {
            Form12 f12 = new Form12();
            f12.Show();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            Form11 f11 = new Form11();
            f11.Show();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            Form10 f10 = new Form10();
            f10.Show();
        }

        private void button15_Click(object sender, EventArgs e)
        {
            Form9 f9 = new Form9();
            f9.Show();
        }

        private void button6_Click_1(object sender, EventArgs e)
        {

        }
    }
}
