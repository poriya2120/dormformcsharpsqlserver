﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FastReport;

namespace WindowsFormsApplication3
{
    public partial class Form10 : Form
    {
        public void cleantextbox()
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            textBox7.Text = "";
            textBox8.Text = "";

        }
        public void serchids()
        {
            try
            {
                var db = new DataClasses1DataContext();
                int n = int.Parse(textBox1.Text);
                var ed = db.factors.Where(y => y.factorid == n).Single();


                textBox2.Text = Convert.ToString(ed.calerkid_FK);
                textBox3.Text = Convert.ToString(ed.studentid_FK);
                textBox4.Text = Convert.ToString(ed.reserveid_FK);
                textBox5.Text = Convert.ToString(ed.roomid_FK);
                textBox6.Text = Convert.ToString(ed.paymentid_FK);
                textBox7.Text = ed.data;

                textBox8.Text = ed.name;
                textBox9.Text = ed.descrptions;







            }
            catch
            {
            }

        }
        public void delete()
        {
            try
            {
                var db = new DataClasses1DataContext();
                int n = int.Parse(textBox1.Text);
                var d = db.factors.Where(y => y.factorid == n);
                if (d.Count() != 0)
                {
                    db.factors.DeleteOnSubmit(d.Single());
                    db.SubmitChanges();

                    cleantextbox();

                    MessageBox.Show(" delete is susees");


                }
                else
                {
                    MessageBox.Show("no rows for delete");
                }
            }
            catch { }
        }
        public void update()
        {
            try
            {
                var db = new DataClasses1DataContext();
                int n = int.Parse(textBox1.Text);
                var ed = db.factors.Where(y => y.factorid == n).Single();
                ed.calerkid_FK = Convert.ToInt16(textBox2.Text);
                ed.studentid_FK = Convert.ToInt16(textBox3.Text);
                ed.reserveid_FK = Convert.ToInt16(textBox4.Text);
                ed.roomid_FK = Convert.ToInt16(textBox5.Text);
                ed.paymentid_FK = Convert.ToInt16(textBox6.Text);

                ed.data = textBox7.Text;
                ed.name = textBox8.Text;
                ed.descrptions = textBox9.Text;




                db.SubmitChanges();
                MessageBox.Show("edit sussfully");
                cleantextbox();
            }
            catch
            {
                MessageBox.Show("لطفا  فیلد ها را ابتدا پر کنید  سپس کلید ویرایش را بزنید");
            }
        }

        public void insert()
        {

            try
            {
                var db = new DataClasses1DataContext();
                factor t = new factor()
                {
                    factorid = Convert.ToInt16(textBox1.Text),
                    calerkid_FK = Convert.ToInt16(textBox2.Text),
                    studentid_FK = Convert.ToInt16(textBox3.Text),
                    reserveid_FK = Convert.ToInt16(textBox4.Text),
                    roomid_FK = Convert.ToInt16(textBox5.Text),
                    paymentid_FK = Convert.ToInt16(textBox6.Text),
                    data = textBox7.Text,
                    name = textBox8.Text,
                    

                    
                    descrptions = textBox9.Text,




                };
                db.factors.InsertOnSubmit(t);
                db.SubmitChanges();
                MessageBox.Show("ثبت شد");
                cleantextbox();
            }
            catch
            {
                MessageBox.Show("ثبت نشد");

            }
        }
        public Form10()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            insert();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            delete();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Report re = new Report();
            re.Load("fac.frx");
            re.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            update();
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            serchids();
        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form10_Load(object sender, EventArgs e)
        {

        }
    }
}
