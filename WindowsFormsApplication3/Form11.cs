﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FastReport;

namespace WindowsFormsApplication3
{
    public partial class Form11 : Form
    {
        public void cleantextbox()
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
        

        }
        public void serchids()
        {
            try
            {
                var db = new DataClasses1DataContext();
                int n = int.Parse(textBox1.Text);
                var ed = db.courses.Where(y => y.courseid == n).Single();
                textBox2.Text = Convert.ToString(ed.studentid_FK);
            


                textBox3.Text = ed.courseName;
                textBox4.Text = ed.courseTitle;
                textBox5.Text = ed.courseTerm;
                textBox6.Text = ed.coursedescrption;
             







            }
            catch
            {
            }

        }
        public void delete()
        {
            try
            {
                var db = new DataClasses1DataContext();
                int n = int.Parse(textBox1.Text);
                var d = db.courses.Where(y => y.courseid == n);
                if (d.Count() != 0)
                {
                    db.courses.DeleteOnSubmit(d.Single());
                    db.SubmitChanges();

                    cleantextbox();

                    MessageBox.Show(" delete is susees");


                }
                else
                {
                    MessageBox.Show("no rows for delete");
                }
            }
            catch { }
        }
        public void update()
        {
            try
            {
                var db = new DataClasses1DataContext();
                int n = int.Parse(textBox1.Text);
                var ed = db.courses.Where(y => y.courseid == n).Single();

                ed.studentid_FK = Convert.ToInt16(textBox2.Text);
                ed.courseName = textBox3.Text;
                ed.courseTitle = textBox4.Text;
                ed.courseTerm = textBox5.Text;
                ed.coursedescrption = textBox6.Text;




                db.SubmitChanges();
                cleantextbox();
            }
            catch
            {
                MessageBox.Show("لطفا  فیلد ها را ابتدا پر کنید  سپس کلید ویرایش را بزنید");
            }
        }

        public void insert()
        {

            try
            {
                var db = new DataClasses1DataContext();
                course t = new course()
                {
                    courseid = Convert.ToInt16(textBox1.Text),
                    studentid_FK = Convert.ToInt16(textBox2.Text),
                    courseName = textBox3.Text,
                    courseTitle = textBox4.Text,
                    courseTerm = textBox5.Text,
                    

                    coursedescrption = textBox6.Text,
                    




                };
                db.courses.InsertOnSubmit(t);
                db.SubmitChanges();
                MessageBox.Show("ثبت شد");
                cleantextbox();
            }
            catch
            {
                MessageBox.Show("ثبت نشد");

            }
        }
        public Form11()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            insert();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Report re = new Report();
            re.Load("curs.frx");
            re.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            update();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            delete();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            serchids();
        }

        private void Form11_Load(object sender, EventArgs e)
        {

        }
    }
}
