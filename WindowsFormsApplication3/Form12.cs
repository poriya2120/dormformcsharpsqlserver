﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FastReport;

namespace WindowsFormsApplication3
{
    public partial class Form12 : Form
    {
        public void cleantextbox()
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            textBox7.Text = "";
            textBox8.Text = "";

        }
        public void serchids()
        {
            try
            {
                var db = new DataClasses1DataContext();
                int n = int.Parse(textBox1.Text);
                var ed = db.payements.Where(y => y.paymentid == n).Single();
                textBox2.Text = Convert.ToString(ed.studentid_FK);
                textBox3.Text = Convert.ToString(ed.reserveid_FK);
                textBox4.Text = ed.paymentName;
                textBox5.Text = ed.bankName;
                textBox6.Text = ed.paymentType;
                textBox7.Text = ed.cost;
                textBox8.Text = ed.descrptions;
                

                







            }
            catch
            {
            }

        }
        public void delete()
        {
            try
            {
                var db = new DataClasses1DataContext();
                int n = int.Parse(textBox1.Text);
                var d = db.payements.Where(y => y.paymentid == n);
                if (d.Count() != 0)
                {
                    db.payements.DeleteOnSubmit(d.Single());
                    db.SubmitChanges();

                    cleantextbox();

                    MessageBox.Show(" delete is susees");


                }
                else
                {
                    MessageBox.Show("no rows for delete");
                }
            }
            catch { }
        }
        public void update()
        {
            try
            {
                var db = new DataClasses1DataContext();
                int n = int.Parse(textBox1.Text);
                var ed = db.payements.Where(y => y.paymentid == n).Single();
                ed.studentid_FK = Convert.ToInt16(textBox2.Text);
                ed.reserveid_FK = Convert.ToInt16(textBox3.Text);
                ed.paymentName = textBox4.Text;
                ed.bankName = textBox5.Text;
                ed.paymentType = textBox6.Text;
                ed.cost = textBox7.Text;
                ed.descrptions = textBox8.Text;




                db.SubmitChanges();
                cleantextbox();
            }
            catch
            {
                MessageBox.Show("لطفا  فیلد ها را ابتدا پر کنید  سپس کلید ویرایش را بزنید");
            }
        }

        public void insert()
        {

            try
            {
                var db = new DataClasses1DataContext();
                payement t = new payement()
                {
                    paymentid = Convert.ToInt16(textBox1.Text),
                    studentid_FK = Convert.ToInt16(textBox2.Text),
                    reserveid_FK = Convert.ToInt16(textBox3.Text),
                    paymentName = textBox4.Text,
                    bankName = textBox5.Text,
                    paymentType = textBox6.Text,
                    cost = textBox7.Text,

                    descrptions = textBox8.Text,
                    




                };
                db.payements.InsertOnSubmit(t);
                db.SubmitChanges();
                MessageBox.Show("ثبت شد");
                cleantextbox();
            }
            catch
            {
                MessageBox.Show("ثبت نشد");

            }
        }
        public Form12()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Report re = new Report();
            re.Load("pay.frx");
            re.Show();
        }

        private void Form12_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            insert();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            update();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            delete();
        }
    }
}
