﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Dapper;
using FastReport;
namespace WindowsFormsApplication3

{
    public partial class Form13 : Form
    {
        SqlConnection con = new SqlConnection("Data Source=.;Initial Catalog=khobgah;Integrated Security=True");
        public void cleantextbox()
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
    


        }
     
        public Form13()
        {
           
            InitializeComponent();
        }
        public void filldata()
        {
            DynamicParameters param = new DynamicParameters();
         
            param.Add("@basporsname", textBox7.Text.Trim());
            List<basporsi> list = con.Query<basporsi>("Sp_serachAllOrSerchIds", param, commandType:
                CommandType.StoredProcedure).ToList<basporsi>();
            dataGridView1.DataSource = list;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (con.State == ConnectionState.Closed)
                    con.Open();
                DynamicParameters param = new DynamicParameters();
                int id = Convert.ToInt32(textBox1.Text);
                int roomid = Convert.ToInt32(textBox2.Text);
                int khobgahid = Convert.ToInt32(textBox3.Text);
                param.Add("@basporsid", id);
                param.Add("@roomid", roomid);
                param.Add("@khobgahid", khobgahid);
                param.Add("@basporsname", textBox4.Text.Trim());
                param.Add("@qualityroom", textBox5.Text.Trim());
                param.Add("@descptions", textBox6.Text.Trim());
                con.Execute("Sp_AddOrEdits", param, commandType: CommandType.StoredProcedure);
                MessageBox.Show("save");
                filldata();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.ToString());
            }
            finally
            {
                con.Close();
            }

        }

        class basporsi
        {
            public int basporsid { get; set; }
            public int roomid { get; set; }
            public int khobgahid { get; set; }
            public string basporsname { get; set; }
            public string qualityroom { get; set; }
            public string descrptions { get; set; }

        }

        private void Form13_Load(object sender, EventArgs e)
        {
            try
            {
                filldata();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                filldata();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            cleantextbox();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox1.Text = dataGridView1.CurrentRow.Cells[0].Value.ToString();
            textBox2.Text = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            textBox3.Text = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            textBox4.Text = dataGridView1.CurrentRow.Cells[3].Value.ToString();
            textBox5.Text = dataGridView1.CurrentRow.Cells[4].Value.ToString();
            textBox6.Text = dataGridView1.CurrentRow.Cells[5].Value.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                DynamicParameters param = new DynamicParameters();
                int id = Convert.ToInt32(textBox1.Text);

                param.Add("@basporsid", id);
                con.Execute("Sp_deleteByIDs", param, commandType: CommandType.StoredProcedure);
                cleantextbox();
                filldata();
                MessageBox.Show("delete");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Report re = new Report();
            re.Load("bas.frx");
            re.Show();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Random r = new Random();
            int re = r.Next(0, 256);
            int blu = r.Next(0, 256);
            int gree = r.Next(0, 256);
            groupBox2.BackColor = Color.FromArgb(re, blu, gree);

        }
    }
}
