﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FastReport;

namespace WindowsFormsApplication3
{
    public partial class Form3 : Form
    {
        public void serchid()
        {
            try
            {
                var db = new DataClasses1DataContext();
                int n = int.Parse(textBox1.Text);
                var ed = db.khabgahs.Where(y => y.khabgahId == n).Single();
                //  ed.facnum = Convert.ToInt16(textBox51.Text);
                textBox2.Text = Convert.ToString(ed.counts);
                textBox3.Text = Convert.ToString(ed.capacity);
                textBox4.Text = ed.typ;
                textBox5.Text = ed.addres;



            }
            catch
            {
            }
        }
        public void cleantextbox()
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            textBox7.Text = "";
            textBox8.Text = "";
        }
        public void delete()
        {
            try
            {
                var db = new DataClasses1DataContext();
                int n = int.Parse(textBox1.Text);
                var d = db.khabgahs.Where(y => y.khabgahId == n);
                if (d.Count() != 0)
                {
                    db.khabgahs.DeleteOnSubmit(d.Single());
                    db.SubmitChanges();
                    cleantextbox();






                }
                else
                {
                    MessageBox.Show("no rows for delete");
                }
            }
            catch { }
        }
        public void update()
        {
            try
            {
                var db = new DataClasses1DataContext();
                int n = int.Parse(textBox1.Text);
                var ed = db.khabgahs.Where(y => y.khabgahId == n).Single();
                //  ed.facnum = Convert.ToInt16(textBox51.Text);
                ed.counts = Convert.ToInt16(textBox2.Text);
                ed.capacity = Convert.ToInt16(textBox3.Text);
                ed.typ = textBox4.Text;
                ed.addres = textBox5.Text;


                db.SubmitChanges();
                cleantextbox();
            }
            catch
            {
                MessageBox.Show("لطفا  فیلد ها را ابتدا پر کنید  سپس کلید ویرایش را بزنید");
            }
        }

        public void insert()
        {

            try
            {
                var db = new DataClasses1DataContext();
                khabgah t = new khabgah()
                {
                    khabgahId = Convert.ToInt16(textBox1.Text),
                    capacity = Convert.ToInt16(textBox2.Text),
                    counts = Convert.ToInt16(textBox3.Text),
                    typ = textBox3.Text,
                    addres = textBox4.Text,


                };
                db.khabgahs.InsertOnSubmit(t);
                db.SubmitChanges();
                MessageBox.Show("ثبت شد");
                cleantextbox();
            }
            catch
            {
                MessageBox.Show("ثبت نشد");

            }
        }
        public Form3()
        {
            InitializeComponent();
        }

        private void Form3_Load(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Report re = new Report();
            re.Load("khobgahs.frx");
            re.Show();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            insert();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            update();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            delete();

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
          serchid();

        }
    }
}
