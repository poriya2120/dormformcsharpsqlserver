﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FastReport;
using Microsoft.Office.Interop.Excel;
using System.Data.SqlClient;

namespace WindowsFormsApplication3
{
    public partial class Form4 : Form
    {
        public void RestoreXml()
        {
            OpenFileDialog open = new OpenFileDialog();
            if (open.ShowDialog() == DialogResult.OK)
            {
                DataSet ds = new DataSet();
                ds.ReadXml(open.FileName);
                dataGridView1.DataSource = ds.Tables[0];

            }

        }
        public void exportXml()
        {
            SaveFileDialog save = new SaveFileDialog();
            if (save.ShowDialog() == DialogResult.OK)
            {
                SqlConnection con = new SqlConnection("Data Source=.;Initial Catalog=khobgah;Integrated Security=True");

                SqlDataAdapter adp = new SqlDataAdapter("ps_showCalerk", con);
                DataSet ds = new DataSet();
                adp.Fill(ds, "t");
                ds.WriteXml(save.FileName);



            }

        }
        public void serchid()
        {
            try
            {
                var db = new DataClasses1DataContext();
                int n = int.Parse(textBox1.Text);
                var ed = db.rezerves.Where(y => y.rezerveId == n).Single();
                //  ed.facnum = Convert.ToInt16(textBox51.Text);
                textBox2.Text = Convert.ToString(ed.roomId);
                textBox3.Text = Convert.ToString(ed.studentId);
                textBox4.Text = Convert.ToString(ed.clerkId);
                textBox7.Text = Convert.ToString(ed.phone);
                textBox5.Text = ed.name;
                textBox6.Text = ed.family;
                textBox8.Text = ed.addres;


            }
            catch
            {
            }
        }
        public void delete()
        {
            try
            {
                var db = new DataClasses1DataContext();
                int n = int.Parse(textBox1.Text);
                var d = db.rezerves.Where(y => y.rezerveId == n);
                if (d.Count() != 0)
                {
                    db.rezerves.DeleteOnSubmit(d.Single());
                    db.SubmitChanges();
                    cleantextbox();

                    MessageBox.Show(" delete");



                }
                else
                {
                   MessageBox.Show("no rows for delete");
                    cleantextbox();
                }
            }
            catch { }
        }
        public void update()
        {
            try
            {
                var db = new DataClasses1DataContext();
                int n = int.Parse(textBox1.Text);
                var ed = db.rezerves.Where(y => y.rezerveId == n).Single();
                ed.roomId = Convert.ToInt16(textBox2.Text);
                ed.studentId = Convert.ToInt16(textBox3.Text);
                ed.clerkId = Convert.ToInt16(textBox4.Text);
                ed.name = textBox5.Text;
                ed.family = textBox6.Text;
                ed.phone = Convert.ToInt16(textBox7.Text);
                
                
                ed.addres = textBox8.Text;


                db.SubmitChanges();
                cleantextbox();
                MessageBox.Show("edit is sussfully");

            }
            catch
            {
            }
        }
        public void cleantextbox()
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            textBox7.Text = "";
            textBox8.Text = "";

        }
        public void insert()
        {

            try
            {
                var db = new DataClasses1DataContext();
                rezerve t = new rezerve()
                {
                    rezerveId = Convert.ToInt32(textBox1.Text),
                    roomId = Convert.ToInt32(textBox2.Text),
                    studentId = Convert.ToInt32(textBox3.Text),
                    clerkId = Convert.ToInt32(textBox4.Text),
                   
                   


                    name = textBox5.Text,
                    family = textBox6.Text,
                    phone = Convert.ToInt16(textBox7.Text),
                    addres=textBox8.Text



                };
                db.rezerves.InsertOnSubmit(t);
                db.SubmitChanges();
                MessageBox.Show("ثبت شد");
                cleantextbox();
            }
            catch
            {

            }
        }
        public Form4()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Report re = new Report();
            re.Load("rezerve.frx");
            re.Show();
        }

        private void Form4_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'khobgahDataSet.rezerve' table. You can move, or remove it, as needed.
            this.rezerveTableAdapter.Fill(this.khobgahDataSet.rezerve);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            update();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            delete();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            insert();

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            serchid();

        }

        private void button5_Click(object sender, EventArgs e)
        {
            exportXml();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            RestoreXml();

        }
    }
}
