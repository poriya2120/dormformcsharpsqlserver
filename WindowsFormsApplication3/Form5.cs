﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FastReport;

namespace WindowsFormsApplication3
{
    public partial class Form5 : Form
    {

        public void serchid()
        {
            try
            {
                var db = new DataClasses1DataContext();
                int n = int.Parse(textBox1.Text);
                var ed = db.rooms.Where(y => y.roomId == n).Single();
                //  ed.facnum = Convert.ToInt16(textBox51.Text);
                textBox2.Text = Convert.ToString(ed.khabgahId);

                textBox3.Text = ed.name;
                textBox4.Text = ed.descript;



            }
            catch
            {
            }
        }
        public void delete()
        {
            try
            {
                var db = new DataClasses1DataContext();
                int n = int.Parse(textBox1.Text);
                var d = db.rooms.Where(y => y.roomId == n);
                if (d.Count() != 0)
                {
                    db.rooms.DeleteOnSubmit(d.Single());
                    db.SubmitChanges();


                    cleantextbox();


                }
                else
                {
                    MessageBox.Show("no rows for delete");
                }
            }
            catch { }
        }
        public void update()
        {
            try
            {
                var db = new DataClasses1DataContext();
                int n = int.Parse(textBox1.Text);
                var ed = db.rooms.Where(y => y.roomId == n).Single();
                //  ed.facnum = Convert.ToInt16(textBox51.Text);
                ed.khabgahId = Convert.ToInt16(textBox2.Text);

                ed.name = textBox3.Text;
                ed.descript = textBox4.Text;



                db.SubmitChanges();
                cleantextbox();
                MessageBox.Show("edit is sussfully");
            }
            catch
            {
                MessageBox.Show("لطفا  فیلد ها را ابتدا پر کنید  سپس کلید ویرایش را بزنید");
            }
        }
        public void cleantextbox()
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
     

        }
        public void insert()
        {

            try
            {
                var db = new DataClasses1DataContext();
                room t = new room()
                {
                    roomId = Convert.ToInt16(textBox1.Text),

                    khabgahId = Convert.ToInt16(textBox2.Text),



                    name = textBox3.Text,
                    descript = textBox4.Text




                };
                db.rooms.InsertOnSubmit(t);
                db.SubmitChanges();
                MessageBox.Show("ثبت شد");
                cleantextbox();
            }
            catch
            {
                MessageBox.Show("ثبت نشد");

            }
        }
        public Form5()
        {
            InitializeComponent();
        }

        private void Form5_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Report re = new Report();
            re.Load("room.frx");
            re.Show();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            insert();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            update();

        }

        private void button5_Click(object sender, EventArgs e)
        {
            delete();

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            serchid();

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            var db = new DataClasses1DataContext();
            int n = int.Parse(textBox1.Text);
            var ed = db.rooms.Where(y => y.roomId == n).Single();
            var bas = db.rooms;
            StringBuilder sb = new StringBuilder();
            sb.Append("<table>");
            sb.Append("<tr><th>roomid</th> <th>khobgahid</th>  <th>name</th>    <th>descrption</th>    </tr>");
            var rows = from r in bas
                       let row = "<td>" + r.roomId + "</td><td>" + r.khabgahId + "</td><td>" + r.name + "</td> <td>" + r.descript + "</td>"
                       select row;
            rows.ToList().ForEach(row => sb.Append("<tr>" + row + "</tr>"));
            sb.Append("</table>");
            System.IO.File.WriteAllText(@"E:\test.html",sb.ToString());

        }
    }
}
