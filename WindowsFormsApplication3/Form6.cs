﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FastReport;
using System.Speech.Synthesis;
using System.IO;

namespace WindowsFormsApplication3
{
    public partial class Form6 : Form
    {
        //public void en_show()
        //{
        //    khobgahEntities contex = new khobgahEntities();
        //    var query = from row in contex.emkanat
        //                select row;
        //    dataGridView1.DataSource = query.ToList();
        //}
        //public void en_insert()
        //{
        //    emkanat em = new emkanat();
        //    em.emkanatId = Convert.ToInt32(textBox1.Text);
        //    em.roomId= Convert.ToInt32(textBox2.Text);
        //    em.descriptions = textBox3.Text;
        //    khobgahEntities contex = new khobgahEntities();

        //    contex.emkanat.Add(em);
        //    contex.SaveChanges();

        //}
        public void serchid()
        {
            try
            {
                var db = new DataClasses1DataContext();
                int n = int.Parse(textBox1.Text);
                var ed = db.emkanats.Where(y => y.emkanatId == n).Single();
                textBox2.Text = Convert.ToString(ed.roomId);

                textBox3.Text = ed.descriptions;





            }
            catch
            {
            }
        }
        public void delete()
        {
            try
            {
                var db = new DataClasses1DataContext();
                int n = int.Parse(textBox1.Text);
                var d = db.emkanats.Where(y => y.emkanatId == n);
                if (d.Count() != 0)
                {
                    db.emkanats.DeleteOnSubmit(d.Single());
                    db.SubmitChanges();
                    cleantextbox();
                    MessageBox.Show(" delete is susees");



                }
                else
                {
                    MessageBox.Show("no rows for delete");
                }
            }
            catch { }
        }
        public void update()
        {
            try
            {
                var db = new DataClasses1DataContext();
                int n = int.Parse(textBox1.Text);
                var ed = db.emkanats.Where(y => y.emkanatId == n).Single();
                ed.roomId = Convert.ToInt16(textBox2.Text);

                ed.descriptions = textBox3.Text;




                db.SubmitChanges();
                cleantextbox();
                MessageBox.Show("edit is susessfully");

            }
            catch
            {
                MessageBox.Show("لطفا  فیلد ها را ابتدا پر کنید  سپس کلید ویرایش را بزنید");
            }
        }
        public void cleantextbox()
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
     

        }
        public void insert()
        {

            try
            {
                var db = new DataClasses1DataContext();
                emkanat t = new emkanat()
                {
                    emkanatId = Convert.ToInt16(textBox1.Text),

                    roomId = Convert.ToInt16(textBox2.Text),



                    descriptions = textBox3.Text,



                };
                db.emkanats.InsertOnSubmit(t);
                db.SubmitChanges();
                MessageBox.Show("ثبت شد");
                cleantextbox();
            }
            catch
            {
                MessageBox.Show("ثبت نشد");

            }
        }
        public Form6()
        {
            InitializeComponent();
        }

        private void Form6_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'khobgahDataSet3.emkanat' table. You can move, or remove it, as needed.
            this.emkanatTableAdapter.Fill(this.khobgahDataSet3.emkanat);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Report re = new Report();
            re.Load("emkanat.frx");
            re.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            insert();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            update();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            delete();

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            serchid();

        }
        SpeechSynthesizer ss;
        private void button5_Click(object sender, EventArgs e)
        {
            ss = new SpeechSynthesizer();
            ss.Rate = trackBar1.Value;
            ss.Volume = trackBar2.Value;
            for (int x = 0; x < dataGridView1.Rows.Count; x++)
            {
                string p = "number" + x.ToString() + "  emkanatid  is" + dataGridView1.Rows[x].Cells[0].Value.ToString() + "roomId is" + dataGridView1.Rows[x].Cells[1].Value.ToString() + "descrption" + dataGridView1.Rows[x].Cells[2].Value.ToString();

                ss.SpeakAsync(p);

            }
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {

        }
    }
}
