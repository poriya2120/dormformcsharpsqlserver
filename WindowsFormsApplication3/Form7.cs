﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FastReport;
using System.Data.Sql;
using System.Data.SqlClient;

namespace WindowsFormsApplication3
{
    public partial class Form7 : Form
    {
        SqlConnection con = new SqlConnection("Data Source=.;Initial Catalog=khobgah;Integrated Security=True");
        public void cleantextbox()
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            textBox7.Text = "";

            textBox8.Text = "";


        }
        public void adoinsert()
        {
           try
            {
              SqlCommand comn = new SqlCommand();
               comn.Connection = con;
                comn.CommandType = CommandType.StoredProcedure;
               comn.CommandText = "insertclercks";
                comn.Parameters.Add("@calerkId", SqlDbType.Int).Value = Convert.ToInt32( textBox1.Text);
                comn.Parameters.Add("@name", SqlDbType.NVarChar, 100).Value = textBox2.Text;
                comn.Parameters.Add("@family", SqlDbType.NVarChar, 100).Value = textBox3.Text;
                comn.Parameters.Add("@username", SqlDbType.NVarChar, 100).Value = textBox4.Text;
              comn.Parameters.Add("@phone", SqlDbType.Int).Value = Convert.ToInt32(textBox5.Text);
                comn.Parameters.Add("@addres", SqlDbType.NVarChar, 100).Value = textBox6.Text;
                comn.Parameters.Add("@descrptions", SqlDbType.NVarChar, 100).Value = textBox7.Text;
                comn.Parameters.Add("@passwords", SqlDbType.NVarChar, 100).Value = textBox8.Text;
         
              comn.Parameters.Add("@r", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
         
                con.Open();
                comn.ExecuteNonQuery();
                if (comn.Parameters["@r"].Value.ToString() == "1")
               {
                
                    MessageBox.Show("ثبت نام انجام شد");

                    cleantextbox();

                }
               else
                {
                    MessageBox.Show("ثبت نام انجام نشد");
                    cleantextbox();


                }
                con.Close();
            }
            catch
           {

              
            }
        }
        //public void adoserchid()
        //{
        //    try
        //    {
        //        SqlCommand comn = new SqlCommand();
        //        comn.Connection = con;
        //        comn.CommandType = CommandType.StoredProcedure;
        //        comn.CommandText = "sel";
        //        comn.Parameters.Add("@dragid", SqlDbType.Int).Value = Convert.ToInt16(textBox1.Text);
        //        comn.Parameters.Add("@dragname", SqlDbType.NVarChar, 100).Direction = ParameterDirection.Output;
        //        comn.Parameters.Add("@r", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        //        con.Open();
        //        comn.ExecuteNonQuery();
        //        if (comn.Parameters["@r"].Value.ToString() == "1")
        //        {
        //            string strname = comn.Parameters["@dragname"].Value.ToString();
        //            MessageBox.Show("the cod is contain the name:" + strname);

        //        }
        //        else
        //        {
        //            MessageBox.Show("no exist cod");
        //        }
        //        con.Close();
        //    }
        //    catch
        //    {
        //        MessageBox.Show(" the search is not coreact");
        //    }
        //}
        public void adodelete()
        {
            try
            {
                SqlCommand comn = new SqlCommand();
                comn.Connection = con;
                comn.CommandType = CommandType.StoredProcedure;
                comn.CommandText = "deleteclercks";
                comn.Parameters.Add("@calerkId", SqlDbType.Int).Value = Convert.ToInt32( textBox1.Text);
                comn.Parameters.Add("@r", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                con.Open();
                comn.ExecuteNonQuery();
                if (comn.Parameters["@r"].Value.ToString() == "0")
                {

                    MessageBox.Show("حذف انجام نشد, لطقا آیدی دارو را در فیلد آیدی وارد کنید");
                    cleantextbox();
                }
                else
                {

                    MessageBox.Show("حذف انجام شد");
                    cleantextbox();

                }
                con.Close();
            }
            catch { }
        }


        public void adoedit()
        {

            try
            {
                SqlCommand comn = new SqlCommand();
                comn.Connection = con;
                comn.CommandType = CommandType.Text;
                comn.CommandType = CommandType.StoredProcedure;


                comn.CommandText = "editclercks";

                comn.Parameters.Add("@calerkId", SqlDbType.Int).Value = Convert.ToInt32(textBox1.Text);
                comn.Parameters.Add("@name", SqlDbType.NVarChar, 100).Value = textBox2.Text;
                comn.Parameters.Add("@family", SqlDbType.NVarChar, 100).Value = textBox3.Text;
                comn.Parameters.Add("@username", SqlDbType.NVarChar, 100).Value = textBox4.Text;
                comn.Parameters.Add("@phone", SqlDbType.Int).Value = Convert.ToInt32(textBox5.Text);
                comn.Parameters.Add("@addres", SqlDbType.NVarChar, 100).Value = textBox6.Text;
                comn.Parameters.Add("@descrptions", SqlDbType.NVarChar, 100).Value = textBox7.Text;
                comn.Parameters.Add("@passwords", SqlDbType.NVarChar, 100).Value = textBox8.Text;
                comn.Parameters.Add("@r", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                con.Open();
                comn.ExecuteScalar();
            
                if (comn.Parameters["@r"].Value.ToString() == "1")
                {
                    MessageBox.Show("ویرایش انجام شد");
                    cleantextbox();

                }
                else
                {

                    MessageBox.Show("ویرایش انجام نشد لطفا کد دارویی که میخواهید ویرایش شود را درفیلد مربوطه وارد کنید");
                    cleantextbox();
                }
                con.Close();
            }
            catch
            {
                MessageBox.Show("لطفا تمام اطلاعات را وارد کنید");

            }


        }

        public void exportExel()
        {

            try
            {
                saveFileDialog1.DefaultExt = "sace execl";
                saveFileDialog1.Filter = "excelfile(*.xlsx)|*.xlsx|exel2003(*.xls)|*.xls";
                saveFileDialog1.OverwritePrompt = true;
                saveFileDialog1.Title = "back up to Exel file";
                saveFileDialog1.FileName = "";
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    MessageBox.Show("dsd");
                    //     filename = saveFileDialog1.FileName;
                    //   System.IO.File.WriteAllText(filename,dataGridView1.DataSource);
                    Microsoft.Office.Interop.Excel.Application exelapp = new Microsoft.Office.Interop.Excel.Application();
                    exelapp.Application.Workbooks.Add(Type.Missing);
                    exelapp.Columns.ColumnWidth = 500;
                    for (int i = 1; i < dataGridView1.Columns.Count; i++)
                    {
                        exelapp.Cells[1, i] = dataGridView1.Columns[i - 1].HeaderText;


                    }
                    for (int x = 0; x < dataGridView1.Rows.Count; x++)
                    {
                        for (int i = 0; i < dataGridView1.Columns.Count; i++)
                        {
                            exelapp.Cells[x + 2, i + 1] = dataGridView1.Rows[x].Cells[i].Value.ToString();



                        }



                    }
                    exelapp.Columns.AutoFit();
                    exelapp.Visible = true;
                    exelapp.ActiveWorkbook.SaveCopyAs(saveFileDialog1.FileName.ToString());
                    exelapp.ActiveWorkbook.Saved = true;
                    exelapp.Quit();

                }
            }
            catch { }






        }
  
       
        public void serchids()
        {
            try
            {
                var db = new DataClasses1DataContext();
                int n = int.Parse(textBox1.Text);
                var ed = db.calerks.Where(y => y.calerkID == n).Single();


                textBox2.Text = ed.name;
                textBox3.Text = ed.family;
                textBox4.Text = ed.username;
                textBox5.Text = Convert.ToString(ed.phone);
                textBox6.Text = ed.addres;
                textBox7.Text = ed.descriptions;

                textBox8.Text = ed.passwords;

              





            }
            catch
            {
            }

        }
        public void delete()
        {
            try
            {
                var db = new DataClasses1DataContext();
                int n = int.Parse(textBox1.Text);
                var d = db.calerks.Where(y => y.calerkID == n);
                if (d.Count() != 0)
                {
                    db.calerks.DeleteOnSubmit(d.Single());
                    db.SubmitChanges();

                    cleantextbox();

                    MessageBox.Show(" delete is susees");


                }
                else
                {
                    MessageBox.Show("no rows for delete");
                }
            }
            catch { }
        }
        public void update()
        {
            try
            {
                var db = new DataClasses1DataContext();
                int n = int.Parse(textBox1.Text);
                var ed = db.calerks.Where(y => y.calerkID == n).Single();


                ed.name = textBox2.Text;
                ed.family = textBox3.Text;
                ed.phone = Convert.ToInt16(textBox4.Text);
                ed.passwords = textBox5.Text;




                db.SubmitChanges();
                cleantextbox();
            }
            catch
            {
                MessageBox.Show("لطفا  فیلد ها را ابتدا پر کنید  سپس کلید ویرایش را بزنید");
            }
        }

        public void insert()
        {

            try
            {
                var db = new DataClasses1DataContext();
                calerk t = new calerk()
                {
                    calerkID = Convert.ToInt16(textBox1.Text),
                    name = textBox2.Text,
                    family = textBox3.Text,
                    username = textBox4.Text,
                    phone = Convert.ToInt16(textBox5.Text),

                    addres = textBox6.Text,
                    descriptions = textBox7.Text,
                    passwords = textBox8.Text,




                };
                db.calerks.InsertOnSubmit(t);
                db.SubmitChanges();
                MessageBox.Show("ثبت شد");
                cleantextbox();
            }
            catch
            {
                MessageBox.Show("ثبت نشد");

            }
        }
        public Form7()
        {
            InitializeComponent();
        }

        private void Form7_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'khobgahDataSet1.calerks' table. You can move, or remove it, as needed.
            this.calerksTableAdapter.Fill(this.khobgahDataSet1.calerks);

        }

        private void button5_Click(object sender, EventArgs e)
        {
            Report re = new Report();
            re.Load("calerk.frx");
            re.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            adoinsert();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            adoedit();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            adodelete();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            serchids();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            exportExel();

        }

        private void button6_Click(object sender, EventArgs e)
        {

           

        }
    }
}
