﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FastReport;

namespace WindowsFormsApplication3
{
    public partial class Form8 : Form
    {
        public void serchids()
        {
            try
            {
                var db = new DataClasses1DataContext();
                int n = int.Parse(textBox1.Text);
                var ed = db.students.Where(y => y.sudentId == n).Single();

                textBox2.Text = Convert.ToString(ed.roomId);

                textBox3.Text = ed.name;
                textBox4.Text   = ed.family;
                textBox5.Text = Convert.ToString(ed.phone);
                textBox6.Text = ed.addres;




            }
            catch
            {
            }

        }
        public void delete()
        {
            try
            {
                var db = new DataClasses1DataContext();
                int n = int.Parse(textBox1.Text);
                var d = db.students.Where(y => y.sudentId == n);
                if (d.Count() != 0)
                {
                    db.students.DeleteOnSubmit(d.Single());
                    db.SubmitChanges();


                    cleantextbox();
                    MessageBox.Show(" delete is sussess");

                }
                else
                {
                    MessageBox.Show("no rows for delete");
                }
            }
            catch { }
        }
        public void update()
        {
            try
            {
                var db = new DataClasses1DataContext();
                int n = int.Parse(textBox1.Text);
                var ed = db.students.Where(y => y.sudentId == n).Single();


                ed.roomId = Convert.ToInt16(textBox2.Text);

                ed.name = textBox3.Text;
                ed.family = textBox4.Text;
                ed.phone = Convert.ToInt16(textBox5.Text);
                ed.addres = textBox6.Text;




                db.SubmitChanges();
                MessageBox.Show("edit sussfully");

                cleantextbox();
            }
            catch
            {
                MessageBox.Show("لطفا  فیلد ها را ابتدا پر کنید  سپس کلید ویرایش را بزنید");
            }
        }
        public void cleantextbox()
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            


        }


        public void insert()
        {

            try
            {
                var db = new DataClasses1DataContext();
                student t = new student()
                {
                    sudentId = Convert.ToInt16(textBox1.Text),
                    roomId = Convert.ToInt16(textBox2.Text),
                    name = textBox3.Text,

                    family = textBox4.Text,

                    phone = Convert.ToInt16(textBox5.Text),

                    addres = textBox6.Text,





                };
                db.students.InsertOnSubmit(t);
                db.SubmitChanges();
                MessageBox.Show("ثبت شد");
                cleantextbox();
            }
            catch
            {
                MessageBox.Show("ثبت نشد");

            }
        }
        public Form8()
        {
            InitializeComponent();
        }

        private void Form8_Load(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Report re = new Report();
            re.Load("student.frx");
            re.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            insert();

        }

        private void button7_Click(object sender, EventArgs e)
        {
            update();

        }

        private void button6_Click(object sender, EventArgs e)
        {
            delete();

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            serchids();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var db = new DataClasses1DataContext();
           // int n = int.Parse(textBox1.Text);
          
            var bas = db.students;
            StringBuilder sb = new StringBuilder();
           
            var rows = from r in bas
                       let row = "studentID" + r.sudentId + "roomId" + r.roomId + "Studentname" + r.name + "studentfamily:" + r.family + "studentphone" +r.phone + "studentaddress:" + r.addres + "----"
                       select row;

            rows.ToList().ForEach(row => sb.Append( row ));
           
            System.IO.File.WriteAllText(@"E:\test2.docx", sb.ToString());
        }
    }
}
