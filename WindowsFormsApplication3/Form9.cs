﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FastReport;

namespace WindowsFormsApplication3
{
    public partial class Form9 : Form
    {
        public void cleantextbox()
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
        

        }
        public void serchids()
        {
            try
            {
                var db = new DataClasses1DataContext();
                int n = int.Parse(textBox1.Text);
                var ed = db.families.Where(y => y.familyid == n).Single();


                textBox2.Text = Convert.ToString(ed.studentid_FK);
                textBox3.Text = Convert.ToString(ed.roomid_FK);
                textBox3.Text = ed.name;
                textBox4.Text = ed.Relate;
               
                textBox5.Text = ed.descrptions;

                







            }
            catch
            {
            }

        }
        public void delete()
        {
            try
            {
                var db = new DataClasses1DataContext();
                int n = int.Parse(textBox1.Text);
                var d = db.families.Where(y => y.familyid == n);
                if (d.Count() != 0)
                {
                    db.families.DeleteOnSubmit(d.Single());
                    db.SubmitChanges();

                    cleantextbox();

                    MessageBox.Show(" delete is susees");


                }
                else
                {
                    MessageBox.Show("no rows for delete");
                }
            }
            catch { }
        }
        public void update()
        {
            try
            {
                var db = new DataClasses1DataContext();
                int n = int.Parse(textBox1.Text);
                var ed = db.families.Where(y => y.familyid == n).Single();

                ed.studentid_FK = Convert.ToInt16(textBox2.Text);
                ed.roomid_FK = Convert.ToInt16(textBox3.Text);
                ed.name = textBox4.Text;
                ed.Relate = textBox5.Text;
                ed.descrptions = textBox6.Text;




                db.SubmitChanges();
                cleantextbox();
            }
            catch
            {
                MessageBox.Show("لطفا  فیلد ها را ابتدا پر کنید  سپس کلید ویرایش را بزنید");
            }
        }

        public void insert()
        {

            try
            {
                var db = new DataClasses1DataContext();
                family t = new family()
                {
                    familyid = Convert.ToInt16(textBox1.Text),
                    studentid_FK = Convert.ToInt16(textBox2.Text),
                    roomid_FK = Convert.ToInt16(textBox3.Text),
                    name = textBox4.Text,
                    Relate = textBox5.Text,
                    descrptions = textBox6.Text,
               




                };
                db.families.InsertOnSubmit(t);
                db.SubmitChanges();
                MessageBox.Show("ثبت شد");
                cleantextbox();
            }
            catch
            {
                MessageBox.Show("ثبت نشد");

            }
        }
        public Form9()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            insert();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            update();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            delete();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Report re = new Report();
            re.Load("family.frx");
            re.Show();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
